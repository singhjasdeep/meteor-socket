import { Meteor } from 'meteor/meteor';
import http from 'http';
import socket_io from 'socket.io';

/******* Defining Port *******/
const PORT = parseInt(process.env.SOCKET_PORT) || 3003;

// Client-side config
WebAppInternals.addStaticJs(`
  window.socketPort = ${PORT};
`);


Meteor.startup(() => {
  // Server Creation
  const server = http.createServer();
  const io = socket_io(server);

  // New client
  io.on('connection', Meteor.bindEnvironment((socket) => {
      /************** Socket event to emit chat messages **************/
    socket.on('chat message', Meteor.bindEnvironment((msg) => {
      /************** Save messages in database **************/
      let res = Message.insert({message:msg,createdAt:Date.now()});
      if(res){
        /******** Emit message back to client *********/
        io.emit('chat message', msg);
      }else{
        /******** Emit error *********/
        io.emit('error');
      }      
     
    }));
  }));

  // Start server
  try {
    server.listen(PORT);
  } catch (e) {
    console.error(e);
  }
});

 
/******** Method to store messages in database *********/
Meteor.methods({
'addMessage':function(value){
  return Message.insert({message:value,createdAt:Date.now()});
}
})
